# 自分用に作成した便利ツール集

## mkmenu.ps1

### 準備

環境変数PATHに、mkmenu.batの存在するディレクトリまでの絶対パスを指定する。

### 使い方

1. Markdown文字列をクリップボードにコピーする。
2. Windowsキー + R を押してから`mkmenu`と実行する。
3. Markdown形式の目次文字列がクリップボードに上書きされる。

