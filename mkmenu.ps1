Add-Type -AssemblyName System.Windows.Forms

# クリップボードの取得
$cp = [Windows.Forms.Clipboard]
$cptext = $cp::GetText()

# 改行文字で区切る
$cptext = $cptext -split "`n"

# クリップボードに追加するテキスト
$menutext = ""
foreach($line in $cptext) {
  if ($line.StartsWith("#")) {
    $cnt = ($line.Length - $line.Replace("#", "").Length)
    $t = $line.Replace("#", "").Trim()
    $title = "[" + $t + "]"

    # #文字の出現回数に応じてインデント
    $indent = ""
    for ($i = 1; $i -lt $cnt; $i++) {
      $indent += "  "
    }
    $txt = $indent + "- " + $title

    # -n オプションが無いときは、目次のリンク文字列を追加する
    if ($args[0] -ne "-n") {
      $txt += "(#" + $t + ")"
    }

    $txt += "`n"

    $menutext += $txt
  }
}

$cp::SetText($menutext)
